﻿namespace BackEnd.Data.Settings
{
    public class JwtSettings
    {
        public virtual string Domain { get; set; }

        public virtual string Secret { get; set; }
    }
}
