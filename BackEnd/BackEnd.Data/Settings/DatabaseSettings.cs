﻿namespace BackEnd.Data.Settings
{
    public sealed class DatabaseSettings
    {
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static DatabaseSettings()
        {
        }

        private DatabaseSettings()
        {
        }

        public static DatabaseSettings Instance { get; } = new DatabaseSettings();
        public string ConnectionString { get; set; }
    }
}