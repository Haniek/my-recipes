﻿using System;
using BackEnd.Data.Entities;

namespace BackEnd.Data.Repositories
{
    public interface IUserAccountRepository
    {
        UserAccount GetUserAccount(Guid id);

        UserAccount GetUserAccount(Func<UserAccount, bool> query);

        void SaveUserAccount(UserAccount userAccount);
    }
}
