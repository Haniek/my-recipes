﻿using System;
using System.Linq;
using BackEnd.Data.Entities;

namespace BackEnd.Data.Repositories
{
    public class UserAccountRepository : IUserAccountRepository
    {
        public UserAccount GetUserAccount(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var userAccount = db.UserAccounts.FirstOrDefault(ua => ua.Id == id);
                return userAccount;
            }
        }

        public UserAccount GetUserAccount(Func<UserAccount, bool> query)
        {
            using (var db = new DatabaseContext())
            {
                var userAccount = db.UserAccounts.FirstOrDefault(query);
                return userAccount;
            }
        }

        public void SaveUserAccount(UserAccount userAccount)
        {
            using (var db = new DatabaseContext())
            {
                db.Add(userAccount);
                db.SaveChanges();
            }
        }
    }
}
