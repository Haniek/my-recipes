﻿using BackEnd.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Data.ModelBuilders
{
    public static class UserAccountModelBuilder
    {
        public static void BuildUserAccountEntity(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserAccount>().ToTable(typeof(UserAccount).Name)
                .HasIndex(a => new {a.UserName, a.EmailAddress})
                .IsUnique();
        }
    }
}