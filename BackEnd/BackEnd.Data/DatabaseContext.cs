﻿using BackEnd.Data.Entities;
using BackEnd.Data.ModelBuilders;
using BackEnd.Data.Settings;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Data
{
    public class DatabaseContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                DatabaseSettings.Instance.ConnectionString,
                b => b.MigrationsAssembly("BackEnd"));

            optionsBuilder.EnableSensitiveDataLogging();
        }

        public DbSet<UserAccount> UserAccounts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.BuildUserAccountEntity();
        }
    }
}