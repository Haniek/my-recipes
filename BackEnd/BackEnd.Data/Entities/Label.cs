﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BackEnd.Data.Entities
{
    public class Label
    {
        public Guid Id { get; set; }

        public Dish Dish { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
