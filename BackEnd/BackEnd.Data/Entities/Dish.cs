﻿using System;
using System.Collections.Generic;

namespace BackEnd.Data.Entities
{
    public class Dish
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<Label> Labels { get; set; }

        public UserAccount UserAccount { get; set; }
    }
}