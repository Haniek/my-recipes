﻿using System.Reflection;
using Backend.Core.Handler;
using BackEnd.Core.Security;
using BackEnd.Data.Repositories;
using BackEnd.Data.Settings;
using BackEnd.Orchestration.Handlers.Account;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;

namespace BackEnd.StartupHelpers
{
    public static class StartupSimpleInjector
    {
        public static void ConfigureSimpleInjector(this IServiceCollection services, Container container)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IControllerActivator>(
                new SimpleInjectorControllerActivator(container));
            services.AddSingleton<IViewComponentActivator>(
                new SimpleInjectorViewComponentActivator(container));

            services.EnableSimpleInjectorCrossWiring(container);
            services.UseSimpleInjectorAspNetRequestScoping(container);
        }

        public static void ConfigureSimpleInjector(this IApplicationBuilder app, Container container, IConfiguration configuration)
        {
            container.RegisterMvcControllers(app);
            container.RegisterMvcViewComponents(app);

            // Add application services. For instance:
            var handlers = new[] {typeof(SubmitLoginHandler).GetTypeInfo().Assembly};
            
            RegisterRepositories(container);

            container.Register(() => configuration.GetSection("JWT").Get<JwtSettings>(), Lifestyle.Singleton);
            container.Register<IClaimsProvider, ClaimsProvider>(Lifestyle.Scoped);
            container.Register<IHandlerDispatcher, HandlerDispatcher>(Lifestyle.Singleton);
            container.Register(typeof(IHandler<,>), handlers);

            // Allow Simple Injector to resolve services from ASP.NET Core.
            container.AutoCrossWireAspNetComponents(app);

            container.Verify();
        }

        private static void RegisterRepositories(Container container)
        {
            container.Register<IUserAccountRepository, UserAccountRepository>(Lifestyle.Scoped);
        }
    }
}