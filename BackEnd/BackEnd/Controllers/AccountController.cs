﻿using Backend.Core.Handler;
using BackEnd.Contract.Account;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IHandlerDispatcher _handlerDispatcher;

        public AccountController(IHandlerDispatcher handlerDispatcher)
        {
            this._handlerDispatcher = handlerDispatcher;
        }

        [HttpPost("[action]")]
        public SubmitLoginResponse SubmitLogin([FromBody] SubmitLoginRequest request)
        {
            return this._handlerDispatcher.Handle(request);
        }

        [HttpPost("[action]")]
        public SubmitCreateAccountResponse SubmitCreateAccount([FromBody] SubmitCreateAccountRequest request)
        {
            return this._handlerDispatcher.Handle(request);
        }
    }
}