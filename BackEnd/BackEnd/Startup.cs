﻿using System.IdentityModel.Tokens.Jwt;
using BackEnd.Data;
using BackEnd.Data.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;
using BackEnd.StartupHelpers;

namespace BackEnd
{
    public class Startup
    {
        private readonly Container _container;
        private IConfiguration Configuration { get; }
        private readonly JwtSettings _jwtSettings;

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
            this._container = new Container();
            this._jwtSettings = this.Configuration.GetSection("JWT").Get<JwtSettings>();

            DatabaseSettings.Instance.ConnectionString =
                this.Configuration.GetSection("ConnectionStrings:DefaultConnection")?.Value;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>();
            services.ConfigerAuthentication(this._jwtSettings);
            services.ConfigureSwagger();
            services.ConfigureSimpleInjector(this._container);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Microsoft overwrites default JWT mapping with their own mapping.
            // By calling the clear method we use the default JWT mapping.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseAuthentication();
            app.ConfigureSimpleInjector(this._container, this.Configuration);

            app.ConfigureSwagger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}