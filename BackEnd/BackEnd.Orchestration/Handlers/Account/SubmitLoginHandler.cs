﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Backend.Core.Handler;
using BackEnd.Contract.Account;
using BackEnd.Data.Entities;
using BackEnd.Data.Repositories;
using BackEnd.Data.Settings;
using BackEnd.Orchestration.Helpers;
using Microsoft.IdentityModel.Tokens;

namespace BackEnd.Orchestration.Handlers.Account
{
    public class SubmitLoginHandler : IHandler<SubmitLoginRequest, SubmitLoginResponse>
    {
        private readonly JwtSettings _jwtSettings;
        private readonly IUserAccountRepository _userAccountRepository;

        public SubmitLoginHandler(JwtSettings jwtSettings, IUserAccountRepository userAccountRepository)
        {
            this._jwtSettings = jwtSettings;
            this._userAccountRepository = userAccountRepository;
        }

        public SubmitLoginResponse Handle(SubmitLoginRequest request)
        {
            var userAccount = this._userAccountRepository.GetUserAccount(ua => ua.UserName == request.UserName);

            if (!DoesAccountExist(userAccount, request.Password))
            {
                throw new Exception("No account exists with this username/password combination.");
            }

            var jwtToken = this.CreateJwtToken(userAccount);
            var tokenString = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return new SubmitLoginResponse
            {
                Token = tokenString
            };
        }

        private static bool DoesAccountExist(UserAccount userAccount, string password)
        {
            if (userAccount == null)
            {
                return false;
            }

            var encryptedInputPassword = PasswordHashHelper.GetHashedPassword(password, userAccount.Salt);

            return encryptedInputPassword == userAccount.Password;
        }

        private JwtSecurityToken CreateJwtToken(UserAccount userAccount)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                this._jwtSettings.Secret
            ));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userAccount.Id.ToString())
            };

            var tokenOptions = new JwtSecurityToken(
                this._jwtSettings.Domain,
                this._jwtSettings.Domain,
                claims,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: signinCredentials
            );

            return tokenOptions;
        }
    }
}