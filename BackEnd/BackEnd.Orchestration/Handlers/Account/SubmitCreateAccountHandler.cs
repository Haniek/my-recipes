﻿using System;
using Backend.Core.Handler;
using BackEnd.Contract.Account;
using BackEnd.Data.Entities;
using BackEnd.Data.Repositories;
using BackEnd.Orchestration.Helpers;

namespace BackEnd.Orchestration.Handlers.Account
{
    public class SubmitCreateAccountHandler : IHandler<SubmitCreateAccountRequest, SubmitCreateAccountResponse>
    {
        private readonly IUserAccountRepository _userAccountRepository;

        public SubmitCreateAccountHandler(IUserAccountRepository userAccountRepository)
        {
            this._userAccountRepository = userAccountRepository;
        }

        public SubmitCreateAccountResponse Handle(SubmitCreateAccountRequest request)
        {
            var existingAccount = this._userAccountRepository.GetUserAccount(ua =>
                ua.EmailAddress == request.EmailAddress || ua.UserName == request.UserName);

            if (existingAccount != null)
            {
                throw new Exception("Account with same username or email exists.");
            }

            var salt = PasswordHashHelper.GenerateSalt();
            var password = PasswordHashHelper.GetHashedPassword(request.Password, salt);

            var newUserAccount = new UserAccount
            {
                Password = password,
                Salt = salt,
                UserName = request.UserName,
                EmailAddress = request.EmailAddress
            };

            this._userAccountRepository.SaveUserAccount(newUserAccount);

            return new SubmitCreateAccountResponse();
        }
    }
}