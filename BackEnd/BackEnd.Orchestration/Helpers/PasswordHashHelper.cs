﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace BackEnd.Orchestration.Helpers
{
    public static class PasswordHashHelper
    {
        public static string GetHashedPassword(string password, byte[] salt)
        {
            // Derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            var hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password,
                salt,
                KeyDerivationPrf.HMACSHA1,
                10000,
                256 / 8));

            return hash;
        }

        public static byte[] GenerateSalt()
        {
            // Generate a 128-bit salt using a secure PRNG
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}