﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace BackEnd.Core.Security
{
    public class ClaimsProvider : IClaimsProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClaimsProvider(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        public bool IsClaimsSet => this.Principal != null && this.GetClaimValue(JwtRegisteredClaimNames.Sub) != null;

        public Guid UserId => Guid.TryParse(this.GetClaimValue(JwtRegisteredClaimNames.Sub), out var result)
            ? result
            : Guid.Empty;

        public string Issuer => this.GetClaimValue(JwtRegisteredClaimNames.Iss);

        private ClaimsPrincipal Principal => this._httpContextAccessor?.HttpContext?.User;

        private string GetClaimValue(string claimKey) => this.Principal.FindFirst(claimKey)?.Value;
    }
}