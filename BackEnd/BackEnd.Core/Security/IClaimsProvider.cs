﻿using System;

namespace BackEnd.Core.Security
{
    public interface IClaimsProvider
    {
        bool IsClaimsSet { get; }

        Guid UserId { get; }

        string Issuer { get; }
    }
}
