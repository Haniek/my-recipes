﻿using SimpleInjector;

namespace Backend.Core.Handler
{
    public class HandlerDispatcher : IHandlerDispatcher
    {
        private readonly Container _container;

        public HandlerDispatcher(Container container)
        {
            this._container = container;
        }

        public TResponse Handle<TResponse>(IRequest<TResponse> request) where TResponse : new()
        {
            var handlerType = typeof(IHandler<,>).MakeGenericType(request.GetType(), typeof(TResponse));
            dynamic handler = this._container.GetInstance(handlerType);
            return handler.Handle((dynamic) request);
        }
    }
}