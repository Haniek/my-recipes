﻿namespace Backend.Core.Handler
{
    public interface IRequest<TResponse> : IQueryArguments<TResponse>
        where TResponse : new()
    {
    }
}