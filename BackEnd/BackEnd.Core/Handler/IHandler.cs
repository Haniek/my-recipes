﻿namespace Backend.Core.Handler
{
    public interface IHandler<in TRequest, out TResponse>
        where TRequest : IRequest<TResponse>
        where TResponse : new()
    {
        TResponse Handle(TRequest request);
    }
}