﻿namespace Backend.Core.Handler
{
    public interface IHandlerDispatcher
    {
        TResponse Handle<TResponse>(IRequest<TResponse> request)
            where TResponse : new();
    }
}