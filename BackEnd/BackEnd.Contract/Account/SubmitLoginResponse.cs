﻿namespace BackEnd.Contract.Account
{
    public class SubmitLoginResponse
    {
        public string Token { get; set; }
    }
}
