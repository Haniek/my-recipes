﻿using System.ComponentModel.DataAnnotations;
using Backend.Core.Handler;

namespace BackEnd.Contract.Account
{
    public class SubmitCreateAccountRequest : IRequest<SubmitCreateAccountResponse>
    {
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; }

        [StringLength(32, MinimumLength = 6)]
        public string Password { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}
