﻿using System;
using BackEnd.Contract.Account;
using BackEnd.Data.Entities;
using BackEnd.Data.Repositories;
using BackEnd.Orchestration.Handlers.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BackEnd.Orchestration.Test.Handlers.Account
{
    [TestClass]
    public class SubmitCreateAccountHandlerTest
    {
        private SubmitCreateAccountHandler _handler;
        private Mock<IUserAccountRepository> _userAccountRepository;
        private SubmitCreateAccountRequest _request;

        [TestInitialize]
        public void Setup()
        {
            this._userAccountRepository = new Mock<IUserAccountRepository>();
            this._handler = new SubmitCreateAccountHandler(this._userAccountRepository.Object);

            this._request = new SubmitCreateAccountRequest
            {
                UserName = "NewUserName",
                EmailAddress = "user@example.com",
                Password = "secret"
            };
        }

        [TestMethod]
        public void CreateAccountSuccessfully()
        {
            this._userAccountRepository.Setup(x => x.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()))
                .Returns<UserAccount>(null);

            this._handler.Handle(this._request);

            this._userAccountRepository.Verify(ua => ua.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()),
                Times.Once);
            this._userAccountRepository.Verify(ua => ua.SaveUserAccount(It.IsAny<UserAccount>()), Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void CreateAccountExceptionAccountExists()
        {
            this._userAccountRepository.Setup(x => x.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()))
                .Returns(new UserAccount());

            this._handler.Handle(this._request);

            this._userAccountRepository.Verify(ua => ua.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()),
                Times.Once);
            this._userAccountRepository.Verify(ua => ua.SaveUserAccount(It.IsAny<UserAccount>()), Times.Never);
        }
    }
}