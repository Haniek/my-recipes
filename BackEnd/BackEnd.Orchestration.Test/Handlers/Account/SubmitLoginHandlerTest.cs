﻿using System;
using BackEnd.Contract.Account;
using BackEnd.Data.Entities;
using BackEnd.Data.Repositories;
using BackEnd.Data.Settings;
using BackEnd.Orchestration.Handlers.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BackEnd.Orchestration.Test.Handlers.Account
{
    [TestClass]
    public class SubmitLoginHandlerTest
    {
        private SubmitLoginHandler _handler;
        private Mock<JwtSettings> _jwtSettings;
        private Mock<IUserAccountRepository> _userAccountRepository;
        private SubmitLoginRequest _request;

        [TestInitialize]
        public void Setup()
        {
            this._jwtSettings = new Mock<JwtSettings>();
            this._userAccountRepository = new Mock<IUserAccountRepository>();
            this._handler = new SubmitLoginHandler(this._jwtSettings.Object, this._userAccountRepository.Object);

            this._request = new SubmitLoginRequest
            {
                UserName = "UserName",
                Password = "Secret"
            };
        }

        [TestMethod]
        public void LoginSuccessfully()
        {
            this._userAccountRepository.Setup(x => x.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()))
                .Returns(new UserAccount
                {
                    Id = Guid.NewGuid(),
                    Salt = new byte[] {51,3,2,1},
                    Password = "vJC75NeGu2v99g1LHhiYHZQpnaEbjqRPglSuPg6iLi8="
                });

            this._jwtSettings.SetupGet(x => x.Secret).Returns("SecretKey1234SecretKey1234");
            this._jwtSettings.SetupGet(x => x.Domain).Returns("localhost");

            var response = this._handler.Handle(this._request);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Token));
            this._userAccountRepository.Verify(ua => ua.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()),
                Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void LoginExceptionAccountDoesNotExist()
        {
            this._userAccountRepository.Setup(x => x.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()))
                .Returns<UserAccount>(null);

            var response = this._handler.Handle(this._request);

            Assert.AreEqual(response, null);
            this._userAccountRepository.Verify(ua => ua.GetUserAccount(It.IsAny<Func<UserAccount, bool>>()),
                Times.Once);
        }
    }
}